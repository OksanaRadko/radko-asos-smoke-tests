package SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TestBootsPage extends BasePage {
    public TestBootsPage (WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//select[@data-id='sizeSelect']")
    private WebElement listOfSizeOfTestBoots;

    @FindBy(xpath = "//select[@id='main-size-select-0']//option[2]")
    private WebElement сhooseSizeOfTestBoots;

    @FindBy(xpath = "//a[@class='add-button']")
    private WebElement addToCartTestBoots;

    @FindBy(xpath = "//span[@class='_1z5n7CN']")
    private WebElement amountOfProductsInCart;

    @FindBy(xpath = "//*[@id=\"chrome-breadcrumb\"]/div/nav/ol/li[2]/a")
    private WebElement mensClothesLink;


    public void clickOnListOfSizeOfTestBoots() {
        listOfSizeOfTestBoots.click();
    }

    public void clickOnChooseSizeOfTestBoots() {
        сhooseSizeOfTestBoots.click();
    }

    public void clickOnAddToCartTestBoots() {
        addToCartTestBoots.click();
    }

    public String getTextOfAmountProductsInCart() {
        return amountOfProductsInCart.getText();
    }

    public void clickOnMensClothesLink() { mensClothesLink.click(); }

}
