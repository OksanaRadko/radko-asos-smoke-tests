package Tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class NegativeTests extends BaseTest {
    private String SEARCH_KEYWORD = "qazwsx";


    @Test
    public void checkThatUrlContainsSearchWord() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        assertTrue(getSearchResultsPage().hasNothingMatchesText());
    }
}
