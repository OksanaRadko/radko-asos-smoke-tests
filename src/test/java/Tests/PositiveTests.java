package Tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class PositiveTests extends BaseTest {
    private String SEARCH_KEYWORD = "dress";
    private String EXPECTED_SEARCH_QUERY = "q=dress";
    private String EXPECTED_AMOUNT_OF_SAVED_ITEMS = "3 items";
    private String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART = "1";
    private String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_AFTER_DELETE = "0";

    @Test(priority = 1)
    public void checkThatUrlContainsSearchWord() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        assertTrue(getDriver().getCurrentUrl().contains(EXPECTED_SEARCH_QUERY));
    }


    @Test(priority = 2)
    public void checkElementsAmountOnSearchPage() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        getBasePage().implicitWait(10);
        assertEquals(getSearchResultsPage().getSearchResultsCount(), 72);
    }

    @Test(priority = 3)
    public void checkThatSearchResultsContainsSearchWord() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        for (WebElement webElement : getSearchResultsPage().getSearchResultsList()) {
            assertTrue(webElement.getText().contains(SEARCH_KEYWORD));
        }
    }

    @Test(priority = 4)
    public void checkElementsAmountOnSearchPageAfterLoadMore() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        getBasePage().implicitWait(10);
        getSearchResultsPage().clickOnLoadMoreButton();
        assertEquals(getSearchResultsPage().getSearchResultsCount(), 144);
    }

    @Test(priority = 5)
    public void checkAddToCart() {
        getHomePage().clickOnMensClothesButton();
        getBasePage().implicitWait(20);
        getMensClothesPage().clickOnMensBootsButton();
        getMensBootsPage().clickOnTestBootsButton();
        getTestBootsPage().clickOnListOfSizeOfTestBoots();
        getTestBootsPage().clickOnChooseSizeOfTestBoots();
        getTestBootsPage().clickOnAddToCartTestBoots();
        assertEquals(getTestBootsPage().getTextOfAmountProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART);
    }

    @Test(priority = 6)
    public void checkAddToCartAndDelete() {
        getHomePage().clickOnMensClothesButton();
        getBasePage().implicitWait(20);
        getMensClothesPage().clickOnMensBootsButton();
        getMensBootsPage().clickOnTestBootsButton();
        getTestBootsPage().clickOnListOfSizeOfTestBoots();
        getTestBootsPage().clickOnChooseSizeOfTestBoots();
        getTestBootsPage().clickOnAddToCartTestBoots();
        getTestBootsPage().clickOnAddToCartTestBoots();
        getTestBootsPage().clickOnMensClothesLink();
        getMensClothesPage().clickOnMiniBagDropdown().;
        getMensClothesPage().clickOnViewBagButton();
        getBagPage().clickOnDeleteItemsButton();
        assertEquals(getBagPage().getTextOfAmountProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_AFTER_DELETE);
    }

    @Test(priority = 7)
    public void checkSortPriceLowToHigh() {
        getHomePage().clickOnWomensClothesButton();
        getBasePage().implicitWait(10);
        getWomensClothesPage().clickOnWomensShortsButton();
        getWomensShortsPage().clickOnSortButton();
        getWomensShortsPage().clickOnPriceLowToHighButton();
        assertEquals(getWomensShortsPage().getProductListPrices().size(), 72);
        List<WebElement> prices = getWomensShortsPage().getProductListPrices();
        for (int i = 0; i < prices.size() - 1; i++) {
            String price0Text = prices.get(i).getText();
            String price1Text = prices.get(i).getText();
            double price0 = Double.parseDouble(price0Text.substring(1));
            double price1 = Double.parseDouble(price1Text.substring(1));
            assertTrue(price0 <= price1);
        }
    }

    @Test(priority = 8)
    public void checkSortPriceHighToLow() {
        getHomePage().clickOnWomensClothesButton();
        getBasePage().implicitWait(10);
        getWomensClothesPage().clickOnWomensShortsButton();
        getWomensShortsPage().clickOnSortButton();
        getWomensShortsPage().clickOnPriceHighToLowButton();
        assertEquals(getWomensShortsPage().getProductListPrices().size(), 72);
        List<WebElement> prices = getWomensShortsPage().getProductListPrices();
        for (int i = 0; i < prices.size() - 1; i++) {
            String price0Text = prices.get(i).getText();
            String price1Text = prices.get(i).getText();
            double price0 = Double.parseDouble(price0Text.substring(1));
            double price1 = Double.parseDouble(price1Text.substring(1));
            assertTrue(price0 >= price1);
        }
    }

    @Test(priority = 9)
    public void checkSavedItems() {
        getHomePage().clickOnWomensClothesButton();
        getBasePage().implicitWait(10);
        getWomensClothesPage().clickOnWomensShortsButton();
        getWomensShortsPage().clickOnAddFirstShortsToFavoriteButton();
        getWomensShortsPage().clickOnAddSecondShortsToFavoriteButton();
        getWomensShortsPage().clickOnAddThirdShortsToFavoriteButton();
        getWomensShortsPage().clickOnGoToSavedItemsButton();
        assertEquals(getSavedItemsPage().getTextOfAmountOfSavedItems(), EXPECTED_AMOUNT_OF_SAVED_ITEMS);
    }
}
