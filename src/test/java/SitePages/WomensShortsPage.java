package SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class WomensShortsPage extends BasePage {

    public WomensShortsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//article[@id='product-14882923']//button[@data-auto-id='saveForLater']")
    private WebElement addFirstShortsToFavoriteButton;

    @FindBy(xpath = "//article[@id='product-20857415']//button[@data-auto-id='saveForLater']")
    private WebElement addSecondShortsToFavoriteButton;

    @FindBy(xpath = "//article[@id='product-21308464']//button[@data-auto-id='saveForLater']")
    private WebElement addThirdShortsToFavoriteButton;

    @FindBy(xpath = "//a[contains(@href,'saved-lists')]")
    private WebElement goToSavedItemsButton;

    @FindBy(xpath = "//li[@data-auto-id='sort']")
    private WebElement sortButton;

    @FindBy(xpath = "//label[@for='plp_web_sort_price_low_to_high']")
    private WebElement priceLowToHighButton;

    @FindBy(xpath = "//label[@for='plp_web_sort_price_high_to_low']")
    private WebElement priceHighToLowButton;

    @FindBy(xpath = "//span[@data-auto-id='productTilePrice']//span")
    private List<WebElement> productListPrices;

    public void clickOnAddFirstShortsToFavoriteButton() {
        addFirstShortsToFavoriteButton.click();
    }

    public void clickOnAddSecondShortsToFavoriteButton() {
        addSecondShortsToFavoriteButton.click();
    }

    public void clickOnAddThirdShortsToFavoriteButton() {
        addThirdShortsToFavoriteButton.click();
    }

    public void clickOnGoToSavedItemsButton() {
        goToSavedItemsButton.click();
    }

    public void clickOnSortButton() {
        sortButton.click();
    }

    public void clickOnPriceLowToHighButton() {
        priceLowToHighButton.click();
    }

    public void clickOnPriceHighToLowButton() {
        priceHighToLowButton.click();
    }

    public List<WebElement> getProductListPrices() {
        return productListPrices;
    }


}
