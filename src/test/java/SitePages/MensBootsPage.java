package SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MensBootsPage extends BasePage {
    public MensBootsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[contains(@href,'pullbear-lace-up-boots')]")
    private WebElement testBootsButton;

    public void clickOnTestBootsButton() {
        testBootsButton.click();
    }
}
