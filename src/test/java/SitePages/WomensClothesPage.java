package SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WomensClothesPage extends BasePage {

    public WomensClothesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[@class='feature__link'][contains(@href,'/women/shorts/cat')]")
    private WebElement shortsButton;

    public void clickOnWomensShortsButton() {
        shortsButton.click();
    }
}
