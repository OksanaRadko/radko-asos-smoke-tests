package SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BagPage extends BasePage {
    public BagPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//button[@class='bag-item-remove']")
    private WebElement deleteItemsButton;

    @FindBy(xpath = "//span[@type='bagUnfilled']")
    private WebElement amountOfProductsInCart;

    public void clickOnDeleteItemsButton() {
        deleteItemsButton.click();
    }

    public String getTextOfAmountProductsInCart() {
        return amountOfProductsInCart.getText();
    }

}
