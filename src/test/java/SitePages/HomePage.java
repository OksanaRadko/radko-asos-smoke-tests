package SitePages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@id='chrome-search']")
    private WebElement searchInput;

    @FindBy(xpath = "//a[@data-testid='women-floor']")
    private WebElement womenGenderButton;

    @FindBy(xpath = "//a[@data-testid='men-floor']")
    private WebElement menGenderButton;

    public void searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword, Keys.ENTER);
    }

    public void clickOnWomensClothesButton() {
        womenGenderButton.click();
    }

    public void clickOnMensClothesButton() {
        menGenderButton.click();
    }

}
