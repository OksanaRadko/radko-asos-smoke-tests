package SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SavedItemsPage extends BasePage {

    public SavedItemsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='itemCount_1ZeG5']")
    private WebElement amountOfSavedItems;

    public String getTextOfAmountOfSavedItems() {
        return amountOfSavedItems.getText();
    }
}