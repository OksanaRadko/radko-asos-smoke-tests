package SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage {
    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//article[@data-auto-id='productTile']")
    private List<WebElement> searchResultsProductsListText;

    @FindBy(xpath = "//a[@data-auto-id='loadMoreProducts']")
    private WebElement loadMoreButton;

    @FindBy(xpath = "//h2[contains(text(), 'NOTHING')]")
    private WebElement nothingMatchesText;

    public List<WebElement> getSearchResultsList() {
        return searchResultsProductsListText;
    }

    public int getSearchResultsCount() {
        return getSearchResultsList().size();
    }

    public void clickOnLoadMoreButton() {
        loadMoreButton.click();
    }

    public boolean hasNothingMatchesText() {
        return nothingMatchesText != null;
    }
}
