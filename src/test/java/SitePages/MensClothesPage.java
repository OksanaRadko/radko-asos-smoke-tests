package SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MensClothesPage extends BasePage {

    public MensClothesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[@class='feature__link'][contains(@href,'/men/shoes-boots')]")
    private WebElement bootsButton;

    @FindBy(xpath = "//div[@id='miniBagDropdown']/a")
    private WebElement miniBagDropdown;

    @FindBy(xpath = "//a[@data-test-id='bag-link'][contains(@href, 'bag')]")
    private WebElement viewBagButton;

    public void clickOnMensBootsButton() {
        bootsButton.click();
    }

    public void clickOnMiniBagDropdown() {
        miniBagDropdown.click();
    }

    public void clickOnViewBagButton() {
        viewBagButton.click();
    }

}

