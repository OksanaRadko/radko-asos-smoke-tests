package Tests;

import SitePages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class BaseTest {
    private WebDriver driver;
    private static final String ASOS_URL = "https://www.asos.com/";


    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(ASOS_URL);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public BasePage getBasePage() {
        return new BasePage(getDriver());
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }

    public SearchResultsPage getSearchResultsPage() {
        return new SearchResultsPage(getDriver());
    }

    public WomensClothesPage getWomensClothesPage() {
        return new WomensClothesPage(getDriver());
    }

    public WomensShortsPage getWomensShortsPage() {
        return new WomensShortsPage(getDriver());
    }

    public SavedItemsPage getSavedItemsPage() {
        return new SavedItemsPage(getDriver());
    }

    public MensClothesPage getMensClothesPage() {
        return new MensClothesPage(getDriver());
    }

    public MensBootsPage getMensBootsPage() {
        return new MensBootsPage(getDriver());
    }

    public TestBootsPage getTestBootsPage() {
        return new TestBootsPage(getDriver());
    }

    public BagPage getBagPage() {
        return new BagPage(getDriver());
    }
}
